<%@ include file="/WEB-INF/include/pageHead.jsp"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="icon" href="/resources/images/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<c:url value='/resources/bootstrap/css/bootstrap.min.css'/>" type="text/css" rel="stylesheet"/>
    <link href="<c:url value='/resources/css/styles.css'/>" type="text/css" rel="stylesheet"/>
    <link href="<c:url value='/resources/css/styles.css'/>" type="text/css" rel="stylesheet" />
    <script src="<c:url value='/resources/js/jquery-2.1.1.min.js'/>" type="application/javascript"></script>
    <script src="<c:url value='/resources/js/admin.js'/>" type="application/javascript"></script>
    <script src="<c:url value='/resources/bootstrap/js/bootstrap.min.js'/>" type="application/javascript"></script>

    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>
</head>
<body>
    Not implemented yet!
</body>

</html>