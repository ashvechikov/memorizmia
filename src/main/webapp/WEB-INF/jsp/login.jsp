<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="true" %>
<html>
<head>
    <title><spring:message code="page.title.login"/></title>
    <meta name="keywords" content="<spring:message code='page.keywords'/>">
    <meta name="description" content="<spring:message code='page.description'/>">
</head>
<body>
<div class="container login-container">
    <div class="login-block">
        <div>
            <h2><spring:message code="enter"/></h2>
            <a id="password-request" href="#" class="grey forgot-link"><spring:message code="forgot.password"/></a>
            <div class="password-request-div">

            </div>
            <div class="clear"></div>
        </div>
        <form action="/j_spring_security_check" class="form-horizontal" method='POST'>
            <div>
                <input type="email" class="inputEmail" name='j_username' autofocus="autofocus" value="${param.email}" placeholder="<spring:message code='your.email' />">
            </div>
            <div>
                <input type="password" class="inputPass" placeholder="<spring:message code='your.pass' />" name='j_password'>
            </div>
            <c:if test="${param.error}">
                <div class="text-error left-align"><spring:message code="AbstractUserDetailsAuthenticationProvider.badCredentials" arguments="${SPRING_SECURITY_LAST_EXCEPTION.message}"/></div>
            </c:if>
            <div class="buttons">
                <div class="checkbox">
                    <input type="checkbox" id="checkbox1" name='_spring_security_remember_me'> <label
                        for="checkbox1"><spring:message code="remember.me"/></label>
                </div>
                <button type="submit" class="btn-enter pull-right"><spring:message code="enter"/></button>
               <div class="clear"></div>
            </div>
        </form>
        <div class="clear"></div>
        <div class="social-block">
            <c:forEach var="network" items="${networks}">
                <a href="${network.loginUrl}" class="${network.type}"></a>
            </c:forEach>
        </div>
    </div>
</div>
<script>
    $(function() {
        Login.init();
    });
</script>
</body>
</html>