<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="yt" uri="/WEB-INF/tlds/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><spring:message code="page.title.404" /></title>
    <meta name="keywords" content="<spring:message code='page.keywords'/>">
    <meta name="description" content="<spring:message code='page.description'/>">
    <link rel="icon" href="/resources/images/loupe/16.png" type="image/x-icon">
    <c:choose>
        <c:when test="${env eq 'DEV'}">
            <link rel="stylesheet/less" type="text/css" href="/resources/less/styles.less">
            <script type="text/javascript">
                less = {
                    env: "development", // or "production"
                    async: false,       // load imports async
                    fileAsync: false,   // load imports async when in a page under
                    poll: 1000         // when in watch mode, time in ms between polls
                };
                localStorage.clear();
            </script>
            <script src="/resources/js/less-1.3.3.min.js"></script>
        </c:when>
        <c:otherwise>
            <link rel="stylesheet" type="text/css" href="/resources/css/styles.css">
        </c:otherwise>
    </c:choose>
</head>
<body>
<sec:authorize access="isAuthenticated()" var="loggedIn" />
<div class="top-menu navbar">
    <div class="container">
        <ul class="nav">
            <li class="mysettings">
                <spring:message code="page.main" var="main"/>
                <yt:urlHandler url="/" text="${main}"/>
            </li>
            <li class="mysettings">
                <spring:message code="search.tasks" var="search_tasks"/>
                <yt:urlHandler url="/search" text="${search_tasks}"/>
            </li>
            <li class="mysettings">
                <spring:message code="employee.search" var="employee_search"/>
                <yt:urlHandler url="/employee-search" text="${employee_search}"/>
            </li>
        </ul>
        <ul class="nav pull-right">
            <c:choose>
                <c:when test="${loggedIn}">
                    <li>
                        <div class="username-block">
                            <div class="username"><a href="/user/${user.id}" >${user.fullName}</a></div>
                            <div class="dropdown-menu">
                                <div class="arrow"></div>
                                <ul class="menu">
                                    <li>
                                        <a class="profile-link" href="/user/${user.id}"><spring:message code="profile" /></a>
                                    </li>
                                    <li>
                                        <a class="tasks-link" href="/user/tasks"><spring:message code="mytasks" /></a>
                                    </li>
                                    <li>
                                        <a class="settings-link" href="/user/settings"><spring:message code="mysettings"/></a>
                                    </li>
                                    <li>
                                        <a class="logout-link" href="/j_spring_security_logout"><spring:message code="logout"/></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <spring:message code="register" var="register"/>
                        <yt:urlHandler url="/register" text="${register}"/>
                    </li>
                    <li>
                        <spring:message code="enter" var="enter"/>
                        <yt:urlHandler url="/login" text="${enter}"/>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<div class="container header">
    <div class="half">
        <a href="/"><img src="/resources/images/logo.png" class="logo"></a>
    </div>
    <div class="half add-task-wrapper">
        <a href="/task/create/" class="btn-warning btn-large">
            <img src="/resources/images/new-task-icon.png"><spring:message code="add.task"/>
        </a>
    </div>
    <div class="clear"></div>
</div>
<div class="container not-found-container">
    <h1><spring:message code="page.404" /></h1>
    <p><spring:message code="page.not.found" /></p>
    <p><spring:message code="can" />
        <c:if test="${not empty referer}">
            <spring:message code="go.back" /> <a href="${referer}"><spring:message code="back" /></a> <spring:message code="or" />
        </c:if>
        <spring:message code="go.to" /> <a href="/"><spring:message code="main.page" /></a></p>
    <div class="not-found"></div>
</div>

<div class="container footer">
    <div class="bottom-menu grey-line">
        <div class="column">
            <ul>
                <c:choose>
                    <c:when test="${loggedIn}">
                        <li><a class="bold" href="/user/${user.id}">${user.fullName}</a></li>
                        <li><a href="/task/create/"><spring:message code="add.task"/></a></li>
                        <li><a href="/user/tasks"><spring:message code="mytasks" /></a></li>
                        <li><a href="/user/settings"><spring:message code="mysettings"/></a></li>
                    </c:when>
                    <c:otherwise>
                        <li><spring:message code="login" /></li>
                        <li><a href="/login"><spring:message code="enter" /></a></li>
                        <li><a href="/register"><spring:message code="register" /></a></li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
        <div class="column">
            <ul>
                <li><spring:message code="search" /></li>
                <li><a href="/search"><spring:message code="search.tasks" /></a></li>
                <li><a href="/employee-search"><spring:message code="employee.search" /></a></li>
            </ul>
        </div>
        <div class="column">
            <ul>
                <li><spring:message code="support" /></li>
                <li><a href="/support/create"><spring:message code="write.us" /></a></li>
            </ul>
        </div>
        <div class="column social-groups">
            <ul>
                <li><spring:message code="we.are.in.social.networks" />:</li>
                <li>
                    <a target="_blank" title="Вконтакте" class="b-share__handle" href="http://vk.com/youtasked"><span class="b-share-icon b-share-icon_vkontakte"></span></a>
                    <a target="_blank" title="Facebook" class="b-share__handle" href="https://www.facebook.com/youtasked"><span class="b-share-icon b-share-icon_facebook"></span></a>
                    <%-- <a target="_blank" title="Twitter" class="b-share__handle" href="#"><span class="b-share-icon b-share-icon_twitter"></span></a>
                     <a target="_blank" title="Одноклассники" class="b-share__handle" href="#"><span class="b-share-icon b-share-icon_odnoklassniki"></span></a>
                     <a target="_blank" title="МойМир" class="b-share__handle" href="#"><span class="b-share-icon b-share-icon_moimir"></span></a>
                     --%><a target="_blank" title="Google+" class="b-share__handle" href="https://plus.google.com/b/105589562294750233643/105589562294750233643"><span class="b-share-icon b-share-icon_gplus"></span></a>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="copyright grey-line">
        &copy; YouTasked, 2013
    </div>
</div>
</body>
</html>