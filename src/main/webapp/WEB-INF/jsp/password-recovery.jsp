<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
    <head>
        <title><spring:message code="password.recovery" /></title>
    </head>
    <body>
        <div class="container main-container">
            <div class="register-block">
                <form method='POST'>
                    <h2><spring:message code="password.recovery" /></h2>
                    <div>
                        <label for="password" class="grey">
                            <spring:message code="password"  />
                        </label>
                        <div>
                            <input type="password" class="inputPass" id="password"  placeholder="<spring:message code="password" />" name='password' autofocus="autofocus" autocomplete="off">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <label for="password2" class="grey">
                            <spring:message code="password2"  />
                        </label>
                        <div>
                            <input type="password" class="inputPass" id="password2"  placeholder="<spring:message code="password2" />" name='password2' autocomplete="off">
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div>
                        <button type="submit" class="register-btn change-pass-btn">
                            <spring:message code="change.password.btn" />
                        </button>
                        <div class="clear"></div>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>