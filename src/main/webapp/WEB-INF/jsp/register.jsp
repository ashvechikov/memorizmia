<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false" %>
<html>
<head>
    <title><s:message code="page.title.registration" /></title>
    <meta name="keywords" content="<spring:message code='page.keywords'/>">
    <meta name="description" content="<spring:message code='page.description'/>">
</head>
<body>
<div class="container register-container">
    <div class="register-block">
        <form:form id="form" method="post" modelAttribute="registrationData">
                <h2><s:message code="register" /></h2>
                <div>
                    <form:label path="email" class="grey">
                        <s:message code="email" />
                    </form:label>
                    <div>
                        <form:input path="email" placeholder="test@example.com" class="inputEmail" />
                        <div class="clear"></div>
                        <form:errors path="email" element="p" cssClass="text-error" />
                    </div>
                </div>
                <div>
                    <form:label path="firstName" class="grey">
                        <s:message code="first.name" var="firstNameText" />
                        ${firstNameText}
                    </form:label>
                    <div>
                        <form:input path="firstName" placeholder="${firstNameText}" class="inputName" />
                        <div class="clear"></div>
                        <form:errors path="firstName" element="p" cssClass="text-error" />
                    </div>
                </div>
                <div>
                    <form:label path="lastName" class="grey">
                        <s:message code="last.name" var="lastNameText" />
                        ${lastNameText}
                    </form:label>
                    <div>
                        <form:input path="lastName" placeholder="${lastNameText}" class="inputName" />
                        <div class="clear"></div>
                        <form:errors path="lastName" element="p" cssClass="text-error" />
                    </div>
                </div>
                <div>
                    <form:label path="password" class="grey">
                        <s:message code="password" var="passwordText" />
                        ${passwordText}
                    </form:label>
                    <div>
                        <form:password path="password" placeholder="${passwordText}" class="inputPass" />
                        <div class="clear"></div>
                        <form:errors path="password" element="p" cssClass="text-error" />
                    </div>
                </div>
                <div>
                    <form:label path="rePassword" class="grey">
                        <s:message code="re.password" var="rePasswordText" />
                        ${rePasswordText}
                    </form:label>
                    <div>
                        <form:password path="rePassword" placeholder="${rePasswordText}" class="inputPass" />
                        <div class="clear"></div>
                        <form:errors path="rePassword" element="p" cssClass="text-error" />
                    </div>
                </div>
                <div>
                    <button type="submit" class="register-btn">
                        <s:message code="register" />
                    </button>
                    <div class="clear"></div>
                </div>
        </form:form>
        <div class="social-block">
            <c:forEach var="network" items="${networks}">
                <a href="${network.loginUrl}" class="${network.type}"></a>
            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>