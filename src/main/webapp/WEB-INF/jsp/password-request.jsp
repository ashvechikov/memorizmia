<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div id="passwordRequest" class="passwordRequest" title="<spring:message code='password.recovery' />">
    <form method='POST' action="/password-request" id="passwordRequestForm">
        <div class="text-error"></div>
        <div>
            <input type="email" id="email" class="inputEmail" placeholder="<spring:message code="email" />" name='userEmail' autofocus="autofocus">
            <button type="submit" class="recovery-btn">
                <spring:message code="password.recovery.btn" />
            </button>
        </div>
    </form>
</div>
<script>
    $(function() {
        Request.init();
    });
</script>