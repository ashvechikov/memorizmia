package com.memorizmia.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.HashMap;
import java.util.Map;

public class UrlUtils {

    public static Map<String, String> parse(String queryString) {
        Map<String, String> map = new HashMap<>();
        if(StringUtils.isEmpty(queryString)) {
            return map;
        }
        String[] pairs = queryString.split("&");
        String[] keyAndValue;
        for (String pair : pairs) {
            keyAndValue = pair.split("=");
            if(keyAndValue.length > 1) {
                map.put(keyAndValue[0], keyAndValue[1]);
            }
        }
        return map;
    }

    public static <T> T parse(String queryString, Class<T> clazz) {
        try {
            T o = clazz.newInstance();
            Map<String, String> properties = parse(queryString);
            BeanUtils.copyProperties(o, properties);
            return o;
        } catch (Exception e) {
            return null;
        }
    }
}
