package com.memorizmia.utils;


public class Patterns {

    public static final String NAME = "^[a-zA-Zа-яА-Я-]{2,20}$";
    public static final String PASSWORD = "^((\\p{Punct})|([a-zA-Z0-9])){5,20}";
    public static final String EMAIL = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String PHONE = "^\\d{3,15}$";
    public static final String SKYPE = "^\\w{6,32}$";
}
