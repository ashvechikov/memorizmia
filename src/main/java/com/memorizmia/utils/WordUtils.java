package com.memorizmia.utils;

public class WordUtils {

    public static final int DEFAULT_CUT_LIMIT = 400;
    public static final int DEFAULT_HIDE_SYMBOL = 'X';

    public static String pluralForm(int count, String form1, String form2, String form5) {
        count = Math.abs(count) % 100;
        int n1 = count % 10;
        if (count > 10 && count < 20) {
            return form5;
        }
        if (n1 > 1 && n1 < 5) {
            return form2;
        }
        if (n1 == 1) {
            return form1;
        }
        return form5;
    }
    public static String escapeText(String text) {
		return escapeQuotes(escapeGTLT(text));
	}
    public static String escapeGTLT(String text) {
		if(text == null || text.isEmpty()) {
			return "";
		}
		return text.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}
    public static String escapeQuotes(String str) {
		if (str == null || str.isEmpty()) {
			return "";
		}
		return str.replaceAll("\"", "&quot;");
	}

    public static String getCutText(String text) {
        return getCutText(text, DEFAULT_CUT_LIMIT);
    }

    public static String getCutText(String text, int cutLimit) {
        if(text.length() > cutLimit) {
            int end = text.substring(0,cutLimit).lastIndexOf(" ");
            return text.substring(0,end)+"...";
        }
        return text;
    }

    public static String getHiddenText(String text, String symbol) {
        return text.replaceAll("\\D|\\d|\\w|\\W", symbol);
    }
}
