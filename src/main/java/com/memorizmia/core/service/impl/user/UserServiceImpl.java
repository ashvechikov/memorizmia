package com.memorizmia.core.service.impl.user;

import com.google.common.collect.Lists;
import com.memorizmia.core.model.user.User;
import com.memorizmia.core.model.user.UserRole;
import com.memorizmia.core.model.user.UserRoles;
import com.memorizmia.core.model.user.register.RegistrationData;
import com.memorizmia.core.repository.user.UserRepository;
import com.memorizmia.core.service.UserService;
import com.memorizmia.utils.Patterns;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import static org.springframework.util.Assert.notNull;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SaltSource saltSource;

    @Override
    public User register(RegistrationData registrationData) {
        notNull(registrationData);
        User user = new User();
        BeanUtils.copyProperties(registrationData, user);
        String cryptedPassword = passwordEncoder.encodePassword(registrationData.getPassword(), saltSource.getSalt(user));
        user.setPassword(cryptedPassword);
        user.setUserRoles(Lists.newArrayList(new UserRole(UserRoles.ROLE_USER)));
        user.setEnabled(true);
        user = userRepository.save(user);
        sendEmailConfirm(user);
        return user;
    }

    @Override
    public User register(User user) {
        notNull(user);
        user.setUserRoles(Lists.newArrayList(new UserRole(UserRoles.ROLE_USER)));
        user.setEnabled(true);
        user.setRegistrationDate(new Date());
        user = userRepository.save(user);
        return user;
    }

    @Override
    public void sendEmailConfirm(User user) {
        user.setEmailConfirmKey(UUID.randomUUID().toString());
        user.setEmailConfirmed(false);
//        emailService.sendEmailConfirm(user);
        save(user);
    }
    @Override
    public User save(User user) {
        notNull(user);
        return userRepository.save(user);
    }

    @Override
    public User findOne(String id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findByEmail(String email){
        if(StringUtils.isEmpty(email) || !Pattern.matches(Patterns.EMAIL, email)){
            return null;
        }
        return userRepository.findByEmail(email);
    }

    @Override
    public User findByEmailConfirmKey(String key) {
        if(StringUtils.isEmpty(key)) {
            return null;
        }
        return userRepository.findByEmailConfirmKey(key);
    }

    @Override
    public User findByPasswordRecoveryKey(String key) {
        if(StringUtils.isEmpty(key)) {
            return null;
        }
        return userRepository.findByPasswordRecoveryKey(key);
    }

    @Override
    public List<User> findByEmailConfirmed(boolean emailConfirmed) {
        return userRepository.findByEmailConfirmed(emailConfirmed);
    }

    @Override
    public User setAvatar(User user) {
        notNull(user);
        if(!user.isAvatar()) {
            user.setAvatar(true);
            user = save(user);
        }
        return user;
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public long count() {
        return userRepository.count();
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }
}
