package com.memorizmia.core.service;

import com.memorizmia.core.model.user.User;
import com.memorizmia.core.model.user.register.RegistrationData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    User register(RegistrationData registrationData);

    User findByEmail(String email);

    User save(User user);

    User findOne(String id);

	User findByEmailConfirmKey(String key);

	void sendEmailConfirm(User user);

	User findByPasswordRecoveryKey(String key);

    User setAvatar(User user);

    List<User> findByEmailConfirmed(boolean emailConfirmed);

    User register(User user);
    Page<User> findAll(Pageable pageable);

    long count();

    Iterable<User> findAll();
}
