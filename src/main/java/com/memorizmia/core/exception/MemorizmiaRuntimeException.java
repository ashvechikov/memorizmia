package com.memorizmia.core.exception;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 11:12
 */
public class MemorizmiaRuntimeException extends RuntimeException {

    private String mnemo;

    public MemorizmiaRuntimeException(String message, String mnemo) {
        super(message);
        this.mnemo = mnemo;
    }

    public MemorizmiaRuntimeException(String message, Throwable cause, String mnemo) {
        super(message, cause);
        this.mnemo = mnemo;
    }

    public String getMnemo() {
        return mnemo;
    }
}
