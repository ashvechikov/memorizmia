package com.memorizmia.core.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 10:21
 */
@Data
@Entity
public class DictionaryPhrase {

    @Id
    @GeneratedValue
    private long id;

    private String phrase;

    @OneToOne
    private Language language;

    @OneToMany
    private List<DictionaryPhrase> synonyms;
}
