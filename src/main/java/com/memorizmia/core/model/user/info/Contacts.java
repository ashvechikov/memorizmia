package com.memorizmia.core.model.user.info;

import com.memorizmia.utils.Patterns;
import com.memorizmia.utils.WordUtils;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class Contacts implements Serializable{

    private static final long serialVersionUID = 1L;

    @Pattern(regexp = Patterns.SKYPE)
    private String skype;

    @Pattern(regexp = Patterns.EMAIL)
    private String contactEmail;

    @Pattern(regexp = Patterns.PHONE)
    private String phone;

    public boolean isAnyExist(){
        return StringUtils.isNotEmpty(skype) || StringUtils.isNotEmpty(contactEmail) || StringUtils.isNotEmpty(phone);
    }

    public String getHiddenEmail(){
        int pos = contactEmail.indexOf("@");
        String text = contactEmail.substring(0,pos);
        return WordUtils.getHiddenText(text, "X")+contactEmail.substring(pos, contactEmail.length());
    }
    public String getHiddenSkype(){
        return WordUtils.getHiddenText(skype,"X");
    }
    public String getHiddenPhone(){
        return WordUtils.getHiddenText(phone,"X");
    }
}
