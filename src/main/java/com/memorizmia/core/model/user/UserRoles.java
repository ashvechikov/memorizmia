package com.memorizmia.core.model.user;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 13:56
 */
public enum UserRoles {

    ROLE_USER,
    ROLE_ADMIN
}
