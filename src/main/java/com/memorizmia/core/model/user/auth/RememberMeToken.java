package com.memorizmia.core.model.user.auth;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class RememberMeToken implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String username;

    private String series;

    private String tokenValue;

    private Date date;
}
