package com.memorizmia.core.model.user;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class UserRole {

    @Id
    @GeneratedValue
    private long id;

    private String role;

    public UserRole(UserRoles userRoles) {
        role = userRoles.name();
    }
}
