package com.memorizmia.core.model.user;

import com.memorizmia.core.model.DictionaryPhrase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 10:21
 */
@Data
@Entity
public class UserDictionaryItem {

    @Id
    @GeneratedValue
    private long id;

    @OneToOne
    private DictionaryPhrase fromPhrase;
    @OneToOne
    private DictionaryPhrase thPhrase;

}
