package com.memorizmia.core.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 10:22
 */
@Data
@Entity
public class Language {

    @Id
    @GeneratedValue
    private long id;

    private String language;
    private String mnemo;
}
