package com.memorizmia.core.repository.user;

import com.memorizmia.core.model.user.UserRole;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 15:09
 */
public interface UserRoleRepository extends CrudRepository<UserRole, Long> {
}
