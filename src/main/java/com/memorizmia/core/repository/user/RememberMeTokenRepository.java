package com.memorizmia.core.repository.user;

import com.memorizmia.core.model.user.auth.RememberMeToken;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RememberMeTokenRepository extends CrudRepository<RememberMeToken, String> {

    RememberMeToken findBySeries(String series);
    List<RememberMeToken> findByUsername(String username);

}
