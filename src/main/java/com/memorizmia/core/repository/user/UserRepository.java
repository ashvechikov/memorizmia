package com.memorizmia.core.repository.user;

import com.memorizmia.core.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 13:42
 */
public interface UserRepository extends JpaRepository<User, String> {

    User findByEmail(String email);

    User findByEmailConfirmKey(String key);

    User findByPasswordRecoveryKey(String key);

    List<User> findByEmailConfirmed(boolean emailConfirmed);
}
