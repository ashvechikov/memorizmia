package com.memorizmia.web.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

public final class HTTPUtils {
	private static final String HEADER_X_FORWARDED_FOR = "X-FORWARDED-FOR";
	private static final Logger LOGGER = Logger.getLogger(HTTPUtils.class);
	private static final String HEADER_USER_AGENT = "user-agent";
	
    public static String getRemoteAddress(HttpServletRequest request) {
        String remoteAddr = request.getRemoteAddr();
        String x = request.getHeader(HEADER_X_FORWARDED_FOR);
        if (x != null) {
            remoteAddr = parseAddress(x);
        }        
        return remoteAddr;
    }

	protected static String parseAddress(String x) {
		try {
			String remoteAddr = x;
			int idx = remoteAddr.indexOf(',');
			if (idx > -1) {
				remoteAddr = remoteAddr.substring(0, idx);			    
			}
			return remoteAddr;
		} catch (Exception e) {
			LOGGER.error("Ошибка определения REMOTE-IP: " + x);
			return x;
		}
	}

	public static String getUserAgent(HttpServletRequest request) {
		try {
			String userAgent = request.getHeader(HEADER_USER_AGENT);
			if(userAgent == null) {
				userAgent = "";
			}
			return userAgent;
		} catch (Exception e) {
			LOGGER.error("Ошибка получения User-Agent: " + e.getMessage());
			return "";
		}
	}

    public static String getCurrentUri(HttpServletRequest request){
        StringBuilder uri = new StringBuilder((String)request.getAttribute("javax.servlet.forward.servlet_path"));
        String queryString = getQueryString(request);
        if(StringUtils.isNotEmpty(queryString)) {
            uri.append("?");
        }
        uri.append(queryString);
        return uri.toString();
    }

    public static String getQueryString(HttpServletRequest request) {
        if("GET".equalsIgnoreCase(request.getMethod())) {
            return request.getQueryString();
        }
        StringBuilder builder = new StringBuilder();
        Enumeration<String> names = request.getParameterNames();
        String key, value;
        while (names.hasMoreElements()) {
            key = names.nextElement();
            value = request.getParameter(key);
            if(StringUtils.isNotEmpty(value)) {
                builder.append(key).append("=").append(value).append("&");
            }
        }
        if(builder.length() > 0 && builder.lastIndexOf("&") == builder.length() - 1) {
            return builder.substring(0, builder.length() - 1);
        }
        return builder.toString();
    }
}