package com.memorizmia.web.utils;

import lombok.Data;
import org.codehaus.jackson.node.ObjectNode;

import java.util.Collection;

@Data
public class JsonResponse {

    private JsonResponseStatus status;
    private Object result;
    private ObjectNode json;

    public JsonResponse(){}

    public JsonResponse(JsonResponseStatus status){
        setStatus(status);
    }

    public JsonResponse(JsonResponseStatus status, Object result){
        setStatus(status);
        setResult(result);
    }

    public JsonResponse(JsonResponseStatus status, String key, String value) {
        setStatus(status);
        ObjectNode json = new ObjectNode(null);
        json.put(key, value);
        setJson(json);
    }
    
    public JsonResponse(ObjectNode result) {
        setStatus(JsonResponseStatus.SUCCESS);
        this.json = result;
    }
    
    public JsonResponse(Collection<String> errorMessages){
        setStatus(JsonResponseStatus.ERROR);
        setResult(errorMessages);
    }
    public String toString() {
        ObjectNode json = new ObjectNode(null);
    	json.put("status", status.name());
    	json.put("result", this.json);
    	return json.toString();
    }
}
