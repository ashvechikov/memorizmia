package com.memorizmia.web.utils;

public class ViewList {

    public static final String PAGE_NOT_FOUND_VIEW = "404";

    public static final String LOGIN_VIEW = "login";
    public static final String PASSWORD_RECOVERY = "password-recovery";

    public static final String HOME_PAGE_VIEW = "home";

}
