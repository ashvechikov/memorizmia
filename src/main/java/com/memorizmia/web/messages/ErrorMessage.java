package com.memorizmia.web.messages;

public class ErrorMessage extends AlertMessage {
	
	public ErrorMessage(String message) {
		super(message);
	}
	
	@Override
	public String getType() {
		return "error";
	}
}
