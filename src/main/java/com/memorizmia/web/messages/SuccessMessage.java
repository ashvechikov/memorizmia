package com.memorizmia.web.messages;

public class SuccessMessage extends AlertMessage {
	
	public SuccessMessage(String message) {
		super(message);
	}
	
	@Override
	public String getType() {
		return "success";
	}
}
