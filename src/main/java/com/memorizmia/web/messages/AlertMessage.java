package com.memorizmia.web.messages;

public class AlertMessage {
	private String message;
	
	public AlertMessage(String message) {
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	public String getType() {
		return "alert";
	}
}
