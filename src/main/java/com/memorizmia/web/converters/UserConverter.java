package com.memorizmia.web.converters;

import com.memorizmia.core.model.user.User;
import com.memorizmia.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

public class UserConverter implements Converter<String, User> {

    @Autowired
    private UserService userService;

    @Override
    public User convert(String source) {
        return userService.findOne(source);
    }
}
