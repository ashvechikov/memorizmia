/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.memorizmia.web.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author valera
 */

public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws IOException, ServletException {
		SavedRequest savedReq = new HttpSessionRequestCache().getRequest(request, response);
		if (savedReq == null) {
			response.sendRedirect(request.getContextPath());
		} else {
			response.sendRedirect(savedReq.getRedirectUrl());
		}
	}
}