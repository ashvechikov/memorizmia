package com.memorizmia.web.auth;

import com.memorizmia.core.model.user.auth.RememberMeToken;
import com.memorizmia.core.repository.user.RememberMeTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 10:56
 */
public class PersistentTokenRepositoryImpl implements PersistentTokenRepository {

    @Autowired
    private RememberMeTokenRepository rememberMeTokenRepository;

    @Override
    public void createNewToken(PersistentRememberMeToken rememberMeToken) {
        RememberMeToken newToken = new RememberMeToken();
        newToken.setUsername(rememberMeToken.getUsername());
        newToken.setSeries(rememberMeToken.getSeries());
        newToken.setTokenValue(rememberMeToken.getTokenValue());
        newToken.setDate(rememberMeToken.getDate());
        this.rememberMeTokenRepository.save(newToken);
    }

    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        RememberMeToken token = this.rememberMeTokenRepository.findBySeries(series);
        if (token != null){
            token.setTokenValue(tokenValue);
            token.setDate(lastUsed);
            this.rememberMeTokenRepository.save(token);
        }
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String series) {
        RememberMeToken token = this.rememberMeTokenRepository.findBySeries(series);
        if(token == null){
            return null;
        }
        return new PersistentRememberMeToken(token.getUsername(), token.getSeries(), token.getTokenValue(), token.getDate());
    }

    @Override
    public void removeUserTokens(String username) {
        List<RememberMeToken> tokens = this.rememberMeTokenRepository.findByUsername(username);
        this.rememberMeTokenRepository.delete(tokens);
    }
}
