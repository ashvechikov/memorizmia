package com.memorizmia.web.controller;

import com.memorizmia.core.model.user.User;
import com.memorizmia.utils.Patterns;
import com.memorizmia.web.utils.JsonResponse;
import com.memorizmia.web.utils.JsonResponseStatus;
import com.memorizmia.web.utils.ViewList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.regex.Pattern;

@Controller
public class LoginController extends AbstractController {
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SaltSource saltSource;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		if (isAutheficated()) {
			return new ModelAndView("redirect:/");
		}
		return new ModelAndView(ViewList.LOGIN_VIEW);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout() {
		return new ModelAndView(ViewList.LOGIN_VIEW);
	}

	@RequestMapping(value = "/login/ajax/password-request", method = RequestMethod.GET)
	public ModelAndView passwordRequest() {
		if (isAutheficated()) {
			return new ModelAndView("redirect:/");
		}
		return new ModelAndView("password-request");
	}


    @RequestMapping(value = "/password-request", method = RequestMethod.POST)
    public @ResponseBody
    JsonResponse passwordRequest(String userEmail){
        User user = userService.findByEmail(userEmail);
        if(user == null || !user.isEnabled()) {
            return new JsonResponse(JsonResponseStatus.ERROR, getMessage("user.not.found"));
        }
//        userService.sendPasswordRecoveryKey(user);
        return new JsonResponse(JsonResponseStatus.SUCCESS, getMessage("password.request.send.success"));
    }

	private boolean isPasswordNotValid(String password) {
		return !Pattern.matches(Patterns.PASSWORD, password);
	}
}
