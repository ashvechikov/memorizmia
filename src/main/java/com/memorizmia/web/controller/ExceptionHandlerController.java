package com.memorizmia.web.controller;

import com.memorizmia.core.exception.MemorizmiaRuntimeException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 11:09
 */
@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(Exception.class)
    public ModelAndView handleExcepion(Exception e) {

        return null;
    }

    @ExceptionHandler(MemorizmiaRuntimeException.class)
    public ModelAndView handleServiceRuntimeExcepion(MemorizmiaRuntimeException e) {

        return null;
    }
}
