package com.memorizmia.web.controller;

import com.memorizmia.core.model.user.User;
import com.memorizmia.core.model.user.register.RegistrationData;
import com.memorizmia.web.messages.SuccessMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class RegisterController extends AbstractController {

    @ModelAttribute
    public void modelAttributes(Model model) {
        super.modelAttributes(model);
        model.addAttribute("title", getMessage("title.register.page"));
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView registerPageRequest(){
        if(isAutheficated()){
            return new ModelAndView("redirect:/");
        }
        return getRegisterModelAndView(new RegistrationData());
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView register(@Valid RegistrationData registrationData, BindingResult bindingResult, final RedirectAttributes redirectAttributes){
        if(bindingResult.hasErrors()){
            return getRegisterModelAndView(registrationData);
        }
        User user = userService.register(registrationData);
        doAutoLogin(registrationData.getEmail(), registrationData.getPassword(), user.getAuthorities());
        redirectAttributes.addFlashAttribute("alert", new SuccessMessage("registration.success"));
        return new ModelAndView("redirect:" + user.getProfileUri());
    }

    private ModelAndView getRegisterModelAndView(RegistrationData data) {
        ModelAndView modelAndView = new ModelAndView("register", "registrationData", data);
        return modelAndView;
    }
}
