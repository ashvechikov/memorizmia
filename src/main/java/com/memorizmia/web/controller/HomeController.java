package com.memorizmia.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by shved90
 * Date: 10.01.15
 * Time: 10:26
 */
@Controller
@RequestMapping("/")
public class HomeController extends AbstractController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView initHome() {
        return new ModelAndView("index");
    }
}
