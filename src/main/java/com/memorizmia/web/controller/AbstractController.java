package com.memorizmia.web.controller;

import com.memorizmia.core.model.user.User;
import com.memorizmia.core.service.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;

public abstract class AbstractController {

    @Autowired
    protected UserService userService;

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected MessageSource messageSource;

    @Autowired
    protected Validator validator;

    @Autowired
    private ViewResolver viewResolver;

    @ModelAttribute
    public void modelAttributes(Model model) {
        User user = getUser();
        model.addAttribute("user", user);
    }

    public String renderHtml(ModelAndView modelAndView) {
        try {
            View view = viewResolver.resolveViewName(modelAndView.getViewName(), getLocale());
            MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
            view.render(modelAndView.getModel(), request, mockHttpServletResponse);
            String html = mockHttpServletResponse.getContentAsString();
            mockHttpServletResponse.getOutputStream().close();
            return html;
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    protected Locale getLocale() {
        return RequestContextUtils.getLocale(request);
    }

    public boolean isAutheficated(){
        return getUser() != null;
    }

    public User getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth == null || auth.getPrincipal() == null){
            return null;
        }
        return userService.findByEmail(auth.getName());
    }

    public Cookie getCookie(String name){
        Cookie[] cookies = request.getCookies();
        if(cookies == null || StringUtils.isEmpty(name)){
            return null;
        }
        for (Cookie cookie : cookies) {
            if(cookie.getName().equals(name)){
                return cookie;
            }
        }
        return null;
    }

    public String getMessage(String code){
        return getMessage(code, null);
    }

    public String getMessage(String code, Object[] objects){
        try{
            return messageSource.getMessage(code, objects, getLocale());
        } catch (Exception e){
            return code;
        }
    }

    public Map<String, String> getMessageErrors(BindingResult result){
        List<FieldError> errors = result.getFieldErrors();
        Map<String, String> messages = new HashMap<>(errors.size());
        for (FieldError error : errors ) {
            messages.put(error.getField() ,messageSource.getMessage(error, getLocale()));
        }
        return messages;
    }

    public <T> List<String> getErrorMessages(Set<ConstraintViolation<T>> violations) {
        if(CollectionUtils.isEmpty(violations)) {
            return new ArrayList<>(0);
        }
        List<String> messages = new ArrayList<>(violations.size());
        for (ConstraintViolation<?> violation : violations) {
            messages.add(getMessage(getSpringMessageCode(violation)));
        }
        return messages;
    }

    private String getSpringMessageCode(ConstraintViolation<?> violation) {
        return String.format("%s.%s.%s", violation.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName(),
                violation.getLeafBean().getClass().getSimpleName().toLowerCase(), violation.getPropertyPath().toString());
    }

    protected void doAutoLogin(String username, String password, Collection<? extends GrantedAuthority> roles) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(username, password, roles));

        // Create a new session and add the security context.
        HttpSession session = request.getSession(true);
        session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
    }
}
